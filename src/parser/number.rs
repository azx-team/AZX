pub struct Number(pub i64);
pub impl Number {
    pub fn new_num(s: &str) -> Self {
        Self(s.parse().unwrap())
    }
}
}